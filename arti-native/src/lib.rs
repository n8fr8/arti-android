// SPDX-FileCopyrightText: 2022 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: MIT

use jni::JNIEnv;
use jni::objects::{JClass, JString};
use jni::sys::jstring;

use arti::socks::run_socks_proxy;
use arti_client::{config::TorClientConfigBuilder, TorClient};
use futures::task::SpawnExt;

use tracing::{info};

use tracing_subscriber::fmt::Subscriber;
use tracing_subscriber::prelude::*;


#[no_mangle]
#[allow(non_snake_case)]
pub extern "system" fn Java_info_guardianproject_arti_ArtiJNI_startArtiProxyJNI(
                                             env: JNIEnv,
                                             _class: JClass,
                                             cacheDir: JString,
                                             stateDir: JString)
                                             -> jstring {
    let cache_dir: String =
        env.get_string(cacheDir).expect("Couldn't get java string!").into();
    let state_dir: String =
        env.get_string(stateDir).expect("Couldn't get java string!").into();

    let runtime = tor_rtcompat::PreferredRuntime::create().expect("could not create tor runtime");

    let config = TorClientConfigBuilder::from_directories(state_dir, cache_dir)
        .build()
        .expect("could not build tor config");

    let client_builder = TorClient::with_runtime(runtime.clone()).config(config);
    let client = client_builder.create_unbootstrapped().expect("could not build tor client");

    runtime.clone().spawn(async {
        run_socks_proxy(runtime, client, 9150).await.expect("could not run socks proxy");
    }).expect("could not spawn");

    let output = env.new_string(format!("arti-native proxy initialized")).expect("Couldn't create java string!");
    return output.into_inner()

}


// this is supposed to forward artis built-in logging to logcat
// https://gitlab.torproject.org/tpo/core/arti/-/blob/main/doc/Android.md#debugging-and-stability
#[no_mangle]
pub extern "system" fn Java_info_guardianproject_arti_ArtiJNI_initLogging(
                                             _env: JNIEnv,
                                             _class: JClass) {

    let layer = tracing_android::layer("rust.arti").expect("couldn't create tracing layer");
    Subscriber::new()
        .with(layer)
        .init(); // this must be called only once, otherwise your app will probably crash
    info!("arti-android native logging initialized");
}
