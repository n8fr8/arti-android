// SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: MIT

package info.guardianproject.arti.sample;

public enum TorConnectionStatus {
    TOR,
    DIRECT,
    ERROR,
}
