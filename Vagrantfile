# SPDX-FileCopyrightText: 2022 Michael Pöhn <michael@poehn.at>
# SPDX-License-Identifier: MIT

Vagrant.configure("2") do |config|
  config.vm.provider :libvirt do |libvirt|
    libvirt.memory = 4096
    libvirt.cpus = 4
  end
  config.vm.provider "virtualbox" do |v|
    v.memory = 4096
    v.cpus = 4
  end
  config.vm.box = "debian/bullseye64"
  config.vm.provision "shell", inline: <<-SHELL
    set -xeuo pipefail

    apt update -yqq
    apt upgrade -yqq
    apt install -yqq curl sdkmanager build-essential

    sdkmanager --install "ndk;22.1.7171670"
    echo 'export NDK_HOME=/opt/android-sdk/ndk/22.1.7171670' >> /home/vagrant/.profile

    sudo -iu vagrant -- bash -c 'curl --proto "=https" --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y'
    sudo -iu vagrant -- rustup target install armv7-linux-androideabi aarch64-linux-android i686-linux-android x86_64-linux-android
    sudo -iu vagrant -- cargo install cargo-ndk
    echo 'export PATH=$PATH:/opt/android-sdk/ndk/22.1.7171670/toolchains/llvm/prebuilt/linux-x86_64/bin' >> /home/vagrant/.profile
  SHELL
end
